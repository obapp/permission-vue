export default [
  {
    url: '/platform/netty/initialization/onlineCount/v1',
    type: 'post',
    response: config => {
      return {
        'data': 100000,
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  }
]
