// https://www.jianshu.com/p/9240d856ee1c
const Socket = {
  init() {
    console.log(this)
    this.WS = new WebSocket('ws://127.0.0.1:502')
    const data = {
      'action': 'login',
      'session': 'loginSession'
    }
    this.send(data)
    this.onError()
    this.onClose()
  },
  /**
   * 发送数据
   * @param data 发送的数据 必须是object类型
   */
  send(data) {
    this.waitForConnection(() => {
      console.log(data)
      this.WS.send(JSON.stringify(data))
      if (typeof callback !== 'undefined') {
        callback()
      }
    }, 1000)
  },
  /**
   * 发送器
   * @param callback 发送数据回调
   * @param interval 未连接情况下，隔1s监听是否已经成功打开连接
   */
  waitForConnection(callback, interval) {
    console.log(this.WS.readyState)
    if (this.WS.readyState === 1) {
      callback()
    } else {
      setTimeout(() => {
        this.waitForConnection(callback, interval)
      }, interval)
    }
  },
  /**
   * 获取数据回调
   * @param callback 回调函数
   */
  onMessage(callback) {
    console.log(this.WS)
    this.WS.onmessage = (res) => {
      console.log(res)
      try {
        const resData = JSON.parse(res.data)
        console.log(resData)
        callback(resData)
        if (resData.action === 'init') {
          this.showGamePage()
        }
      } catch (e) {
        console.log(e)
      }
    }
  },
  // 获取失败回调
  onError() {
    this.WS.onerror = (res) => {
      console.log('连接错误，请重新刷新页面!')
    }
  },
  // 关闭连接回调
  onClose() {
    this.WS.onclose = (res) => {
    }
  },
  // 进入游戏页面
  showGamePage() {
  }
}
export default Socket
