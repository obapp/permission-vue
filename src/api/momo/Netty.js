import request from '@/utils/request'

// 实时在线人数初始化
export function onlineCount(data) {
  return request({
    url: '/platform/netty/initialization/onlineCount/v1',
    method: 'post',
    data
  })
}

